package GCPlugins::GCgames::GCAmazonFR;

###################################################
#
#  Copyright 2005-2010 Christian Jodar
#  Copyright 2017-2022 Kerenoc
#
#  This file is part of GCstar.
#
#  GCstar is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  GCstar is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with GCstar; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
#
###################################################

use strict;
use utf8;

use GCPlugins::GCgames::GCgamesAmazonCommon;

{
    package GCPlugins::GCgames::GCPluginAmazonFR;

    use base 'GCPlugins::GCgames::GCgamesAmazonPluginsBase';

    sub new
    {
        my $proto = shift;
        my $class = ref($proto) || $proto;
        my $self  = $class->SUPER::new();
        bless ($self, $class);

        $self->{suffix} = 'fr';

        $self->initTranslations;
        
        return $self;
    }

    sub initTranslations
    {
        my $self = shift;
        
        $self->{translations} = {
            publisher     => "(Editeur|Édition)",
            publication   => "Date de publication :",
            language      => "Langue :",
            isbn          => "ISBN-13",
            dimensions    => "Dimensions du produit:",
            series        => "Collection",
            pages         => "pages",
            by            => "de",
            product       => "(Informations sur le produit|Description du produit|Description du fabricant|Détails sur le produit)",
            details       => "Descriptif technique",
            additional    => "Informations complémentaires",
            end           => "Votre avis",
            sponsored     => "Sponsorisé",
            description   => "Description",
            author        => "Auteur",
            translator    => "Traduction",
            artist        => "Illustration",
            platform      => "Plate-forme",
            end           => "(Questions et r|Produits li|Commentaires clients),"
        };
    }

    sub getName
    {
        return 'Amazon (FR)';
    }
    
    sub getLang
    {
        return 'FR';
    }

}

1;
