#! bash
#
# computeDigests.sh
#
# script to compute the MD5 digests of Perl file for uppdating GCstar
#
#
rm list_file_digests.txt
find . -name "*.pm" | \
	while read f
	do
		md5sum < "$f" | \
			sed "s/ .*//" | \
			sed "s,^,$f|," | \
			sed "s,^./,," >> list_file_digests.txt
	done


